﻿SUBROUTINE evol(dt)
use variables
implicit none

double precision,intent(in) :: dt     ! Timestep
integer,dimension(Nvar) :: indx       !
double precision :: wmin,wmax
double precision,dimension(Nvar,Nvar) :: Jact,sav_jact ! Jacobian
double precision,dimension(Nvar) :: b                  ! Right-hand side of the equation
double precision,dimension(Nvar,Nvar) :: VT,U          ! For DGESVD
integer :: k,i,j,l 
integer :: ILO,IHI,INFO
double precision,dimension(Nvar) :: scal,sig     
double precision,dimension(nvar*nvar) :: work
double precision,dimension(Nvar) :: ccc   ! To check the abundance threshold


call Fonctions()    ! call F et Jac
Jact(:,:)= 0.d0

! Threshold in abundance
ccc(:)=1d0
do i=1,Nvar
  if(x(i) < thresh_qte) ccc(i)=0d0
end do

do i=1,Nvar
  do j=1,Nvar
      jact(i,j) = id(i,j) - dt*Jac(i,j)*ccc(i)*ccc(j)
      sav_jact(i,j)=jact(i,j)
  end do
end do
 
do  i=1,nvar
    b(i) = F(i)*dt*ccc(i)
end do

call DGESVD( 'A', 'A', Nvar, Nvar, Jact, Nvar, Sig, U, Nvar, VT, Nvar, WORK, 5*nvar*nvar, INFO )

wmax=0.d0
do  j=1,nvar
    if (Sig(j) .gt. wmax) wmax=Sig(j)
end do
wmin=wmax*1.d-11
do  j=1,nvar
    if (Sig(j).lt.wmin) then
        Sig(j)=0.d0
    end if
end do
  
call svbksb(U,sig,VT,nvar,b,deltx)

do i=1,Nvar
  deltx(i)=deltx(i)*ccc(i)
end do

end subroutine evol


subroutine svbksb(u,w,vt,n,b,deltx)
implicit none
double precision, dimension(n,n)  :: u,vt
double precision, dimension(n)  :: w,b,tmp,deltx
double precision   :: s

integer :: i,j,n

do  j=1,n
    s=0.d0
    if  (w(j).ne.0.d0) then
        do  i=1,n
            s=s+u(i,j)*b(i)
        end do
        s=s/w(j)
    end if
    tmp(j)=s
end do
do  j=1,n
    s=0.d0
    do  i=1,n
        s=s+vt(i,j)*tmp(i)
    end do
    deltx(j)=s
end do


end subroutine svbksb


subroutine equil()
! This routine check the ionisation equilibrium
! And balance with electrons
use variables
implicit none
double precision :: xg0_tot,xgm_tot,xgp_tot,ionis
integer  :: i,j


xg0_tot = 0.d0
xgp_tot = 0.d0
xgm_tot = 0.d0
do  j=1,alpha
    xg0_tot = xg0_tot+x(nion+3+(j-1)*3)
    xgp_tot = xgp_tot+x(nion+1+(j-1)*3)
    xgm_tot = xgm_tot+x(nion+2+(j-1)*3)
end do
ionis = xgp_tot+sum(x(2:nion))-xgm_tot-x(1)
if  (abs(ionis) .gt. maxval(x(1:nvar))*1.d-16) then
    x(1) = xgp_tot+sum(x(2:nion))-xgm_tot
end if


end subroutine equil

