  FC = gfortran
  #FOPT = -g3 -O0 -Wall -Wextra -fbounds-check -fimplicit-none -std=f95 -pedantic -Waliasing
# FOPT = -g3 -O0 -Wall -Wextra -fbounds-check -fimplicit-none -std=f95 -pedantic -Waliasing -fdefault-real-8
# FOPT = -g3 -O0 -Wall -Wextra -fbounds-check -fimplicit-none -std=f95 -pedantic -Wconversion -Waliasing
# FOPT = -g -Wall
  FOPT = -g -O2
  
  OBJ = variables.o \
        grains.o \
        Fonctions.o \
        Reactions.o \
        evol.o \
        resist.o \
        init.o \
        eos.o \
        qte_grains.o\
        calc_mag.o\
        alloc.o\
        read_params.o
  
all: Abondances

variables.o: variables.f90
	$(FC) $(FOPT) -c variables.f90

Fonctions.o: Fonctions.f90
	$(FC) $(FOPT) -c Fonctions.f90

grains.o: grains.f90
	$(FC) $(FOPT) -c grains.f90

Reactions.o: Reactions.f90
	$(FC) $(FOPT) -c Reactions.f90

evol.o: evol.f90
	$(FC) $(FOPT) -c evol.f90
 
resist.o: resist.f90
	$(FC) $(FOPT) -c resist.f90   

init.o: init.f90
	$(FC) $(FOPT) -c init.f90
    
eos.o: eos.f90
	$(FC) $(FOPT) -c eos.f90
    
qte_grains.o: qte_grains.f90
	$(FC) $(FOPT) -c qte_grains.f90

calc_mag.o: calc_mag.f90
	$(FC) $(FOPT) -c calc_mag.f90

alloc.o: alloc.f90
	$(FC) $(FOPT) -c alloc.f90

read_params.o: read_params.f90
	$(FC) $(FOPT) -c read_params.f90

Abondances: $(OBJ) Abondances.f90
	$(FC) $(OPTIONS) $(OBJ) -llapack Abondances.f90 -o Abondances

clean:
	rm -f *.o *.res *.mod 
