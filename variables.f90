module variables
implicit none

! Parameters
integer   :: alpha      ! Number of bins
logical :: active_vap=.TRUE.          ! Grain evaporation
logical :: act_eos=.FALSE.            ! Equation of state
integer   :: mag=0                    ! 0 : B prop to sqrt(nH), 1 : Increase in 4 steps (Tomida 2014)
double precision :: grains_pow_law=-3.5d0    ! Power law for grains distribution
logical :: detail=.FALSE.             ! Writing in res.dat the time evolution of the species
logical :: restart=.FALSE.            ! Restart from last point

double precision :: rho_min = 3d2     ! Min density 
double precision :: rho_max = 1d25    ! Max density
integer          :: ltot = 200        ! Number of density steps 
double precision :: Tmin = 1d1        ! Min temperature
double precision :: Tmax = 5d3        ! Max temperature
integer          :: Ttot = 60         ! Number of temperature steps
double precision :: xi = 1d-17        ! Ionisation rate of an hydrogene molecule





! Numbers
integer  :: nion   ! Number of ions
integer  :: Nvar   ! Number of reacting species (electrons + ions + grains)
integer  :: Ntot   ! Total number of species

!!!! Reactions matrixes
double precision, dimension(:,:), allocatable  :: alp     ! Creation of i with ionisation of j
double precision, dimension(:,:), allocatable  :: gam     ! Destruction of i with reaction i + j -> ... 
double precision, dimension(:,:,:), allocatable :: bet    ! Creation of i with reaction j + k -> i + ...

!!!! Functions and Jacobian
double precision, dimension(:), allocatable   :: F    ! Abundance for each ionised species
double precision, dimension(:,:), allocatable :: Jac  ! Jacobian
double precision, dimension(:,:), allocatable :: Id   ! Identity matrix
double precision, dimension(:,:), allocatable :: perm ! Permutation matrix

!!!! Integration parameter
double precision  :: nH    ! Hydrogen density in particle per cc


!!!! Reaction coefficients
double precision :: G1,G2,G3                    ! H2 or He ionisation
double precision :: G4,G5,G6                    ! K,H and Na ionisation
double precision :: Ba,Bb,Bc                    ! H+
double precision :: Bd,Be,Bf                    ! He+
double precision :: Bg,Bh,Bi,Bj                 ! H3+ 
double precision :: Bk,Bl,Bm,Bn                 ! C+ 
double precision :: Bo                          ! m+ M charge exchange 
double precision :: Bp,Bq,Br,Bs,Bt,Bu,Bv        ! Recombination
double precision :: max_gi,max_gpgm,max_gg0

!!!! Variables
double precision :: T                     ! Temperature in Kelvin
double precision :: delta1 = 2d-1         ! Abundance coefficient in gazeous phase
double precision :: delta2 = 2d-2         ! Abundance coefficient in grains phase
double precision :: fO2                   ! Oxygene fraction in O2 form
double precision :: xH2,xHe,xC,xO,xO2,xM  ! H2, He, etc. abundances
double precision :: xg, xg0               ! Grains abundances
double precision :: xNa,xMg,xAl,xCa,xFe,xNi,xSi,xK   ! Metals abundances
double precision, dimension(:),allocatable :: x_g        ! Grains abundances per bins
double precision, dimension(:),allocatable :: r_g        ! Grains radius per bins
double precision, dimension(:),allocatable :: m_g        ! Grains mass per bins


!!!! For grains collisions
double precision, parameter :: pi=3.1415927410125732422  
double precision, parameter :: rg=0.1d-4          ! Grains radius (cm)
double precision, parameter :: mp=1.6726d-24      ! Proton mass, g
double precision, parameter :: me=9.1094d-28      ! Electron mass, en g
double precision, parameter :: mg=1.2566d-14      ! Grains mass, en g
double precision, parameter :: e=4.803204d-10     ! Electron charge, cgs
double precision, parameter :: mol_ion=29.*mp     ! Molecular ion mass
double precision, parameter :: Met_ion=23.5*mp    ! Atomic ion mass
double precision, parameter :: kB = 1.3807d-16    ! Boltzman constant erg/K
double precision, parameter :: hp = 6.62d-27      ! Planck constant erg.s
double precision, dimension(:), allocatable :: q  ! charge 
double precision, dimension(:), allocatable :: m  ! mass

!!!! Grains bins of size (Power Law)
double precision, parameter :: rho_s=2.3           ! Slicates density, g/cc
double precision, parameter :: rho_n_tot=1.17d-21  ! Total density, g/cc
double precision, parameter :: a_0=0.0375d-4       ! Medium radius, cm
double precision, parameter :: a_min=0.0181d-4     ! Minimum radius, cm
double precision, parameter :: a_max=0.9049d-4     ! Maximum radius, cm
double precision, parameter :: zeta=a_min/a_max    ! a_min/a_max
double precision            :: rho_gtot            ! Total grain density
double precision, parameter :: P_e=0.6d0
double precision, parameter :: P_I=1.0d0


! resistivites (cf Kunz & Mouschovias 2009)
double precision, dimension(:), allocatable  :: sigma                   ! Conductivities
double precision, dimension(:), allocatable  :: zetas                   ! For calculation
double precision, dimension(:), allocatable  :: phi                     ! For calculation
double precision, dimension(:), allocatable  :: tau_sn,tau_gp,tau_gm    ! For calculation
double precision, dimension(:,:), allocatable  :: tau_inel              ! For calculation
double precision, dimension(:), allocatable  :: gamma_zeta,gamma_omega  ! For calculation
double precision, dimension(:), allocatable  :: omega,omega_bar         ! For calculation
double precision, parameter        :: c_l=299792458.d2                  ! Light speed in cm/s


! Abundances
double precision, dimension(:),allocatable   :: x         ! Abundances
double precision, dimension(:),allocatable   :: deltx     ! Abundances variation
double precision, dimension(:),allocatable   :: jco       ! dx/x
double precision     :: thresh_qte = 1d-25                ! Abundance treshold



! Magnetic field
double precision :: scale_d = 3.87e-24      ! For choice 1
double precision :: pente=0.75d0            ! For choice 1
double precision :: thres=1.5d-14           ! For choice 1
double precision, dimension(5) :: rhom=(/ 6.02d5, 6.02d8, 6.02d12, 6.02d14, 6.02d22 /) ! For choice 2
double precision, dimension(5) :: bmagm=(/ 1.9d-5, 4d-3, 1.26d-1, 3.79d-1, 3.16d-4 /) ! For choice 2



end module variables
