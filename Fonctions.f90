subroutine fonctions()
use variables
implicit none

integer :: i,j,k

F(:) = 0.d0
Jac(:,:) = 0.d0


do  i=1,nvar
    do  j=1,Ntot
        do  k=1,Ntot
            F(i) = F(i) + 0.5d0*bet(i,j,k)*x(j)*x(k)*(nH/xi)
        end do
        F(i) = F(i) + alp(i,j)*x(j) - gam(i,j)*x(i)*x(j)*(nH/xi)
    end do
end do

do  i=1,alpha
    F(nion+3+3*(i-1))=-F(nion+1+3*(i-1))-F(nion+2+3*(i-1))
end do

do  i=1,Nvar-1
    do  j=1,Nvar
        do  k=1,Ntot
            Jac(i,j) = Jac(i,j) + bet(i,j,k)*x(k)*(nH/xi)
            if  (i==j) then  
                Jac(i,j) = Jac(i,j) - gam(i,k)*x(k)*(nH/xi)
            end if
        end do
        Jac(i,j) = Jac(i,j) + alp(i,j) - gam(i,j)*x(i)*(nH/xi)
    end do
end do

do  i=1,alpha
    Jac(nion+3+3*(i-1),:)=-Jac(nion+1+3*(i-1),:)-Jac(nion+2+3*(i-1),:)
end do


end subroutine fonctions
