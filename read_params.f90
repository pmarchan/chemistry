subroutine read_params(infile)
USE variables
IMPLICIT NONE
character(80),intent(in) :: infile

! Namelist definition

namelist/physics_parameters/alpha,active_vap,act_eos,mag,grains_pow_law,detail,restart
namelist/run_parameters/rho_min,rho_max,ltot,Tmin,Tmax,Ttot,xi


open(55,file=infile,status='old')
read(55,NML=physics_parameters)
rewind(55)
read(55,NML=run_parameters)

end subroutine

