subroutine alloc()
use variables
IMPLICIT NONE


allocate(F(Nvar))
allocate(Jac(Nvar,Nvar))
allocate(Id(Nvar,Nvar))
allocate(perm(Nvar,Nvar))
allocate(q(Nvar))
allocate(m(Nvar))
allocate(sigma(Nvar))
allocate(zetas(Nvar))
allocate(phi(Nvar))
allocate(tau_sn(Nvar))
allocate(tau_gp(Nvar))
allocate(tau_gm(Nvar))
allocate(gamma_zeta(Nvar))
allocate(gamma_omega(Nvar))
allocate(omega(Nvar))
allocate(omega_bar(Nvar))
allocate(tau_inel(Nvar,Nvar))

allocate(alp(Ntot,Ntot))
allocate(bet(Ntot,Ntot,Ntot))
allocate(gam(Ntot,Ntot))

allocate(x(Ntot))
allocate(deltx(Nvar))
allocate(jco(Nvar))

allocate(x_g(alpha),r_g(alpha),m_g(alpha))



end subroutine alloc
