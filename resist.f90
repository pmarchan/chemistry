subroutine resist(sigP,sigO,sigH,B)
use variables
implicit none
double precision,intent(out)   :: sigP,sigO,sigH   ! conductivities parallele, orthogonale, et hall
double precision, intent(in)   :: B                ! Magnetic field
double precision :: sigv                           ! Collision rate
double precision :: muuu                           ! Reduced mass
integer :: i,j,k

tau_sn(:)      = 0.d0
sigma(:)       = 0.d0
phi(:)         = 0.d0
zetas(:)        = 0.d0
gamma_zeta(:) = 0.d0
gamma_omega(:) = 0.d0
omega_bar(:)   = 0.d0


! See Kunz & Mouschovias (2010) + Pinto & Galli (2008) 
do  i=1,nvar-3*alpha
    if  (i==1) then  ! electron
        sigv=3.16d-11 * (dsqrt(8d0*kB*1d-7*T/(pi*me*1d-3))*1d-3)**1.3d0
        tau_sn(i) = 1.d0/1.16d0*(m(i)+2.d0*mp)/(2.d0*mp)*1.d0/(nH/2.d0*sigv)
    else if (i>=2 .and. i<=nion) then ! ions
        muuu=m(i)*2d0*mp/(m(i)+2d0*mp)
        if (i==2 .or. i==3) then
          sigv=2.4d-9 * (dsqrt(8d0*kB*1d-7*T/(pi*muuu*1d-3))*1d-3)**0.6d0
        else if (i==4) then
          sigv=2d-9 * (dsqrt(8d0*kB*1d-7*T/(pi*muuu*1d-3))*1d-3)**0.15d0
        else if (i==5) then
          sigv=3.89d-9 * (dsqrt(8d0*kB*1d-7*T/(pi*muuu*1d-3))*1d-3)**(-0.02d0)
        else
          sigv=1.69d-9
        end if
        tau_sn(i) = 1.d0/1.14d0*(m(i)+2.d0*mp)/(2.d0*mp)*1.d0/(nH/2.d0*sigv)
    end if
    omega(i) = q(i)*B/(m(i)*c_l)
    sigma(i) = x(i)*nH*(q(i))**2*tau_sn(i)/m(i)
    phi(i) = 0.d0
    zetas(i) = 0.d0
    gamma_zeta(i) = 1.d0
    gamma_omega(i) = 1.d0
    omega_bar(i) = 0.d0

end do

do  i=1,alpha   ! grains
    tau_sn(nion+1+3*(i-1))=1.d0/1.28d0*(m_g(i)+2.d0*mp)/(2.d0*mp)*1.d0/(nH/2.d0*(pi*r_g(i)**2*(8.d0*Kb*T/(pi*2.d0*mp))**0.5))    ! g+
    omega(nion+1+3*(i-1)) = q(nion+1+3*(i-1))*B/(m_g(i)*c_l)
    sigma(nion+1+3*(i-1)) = x(nion+1+3*(i-1))*nH*(q(nion+1+3*(i-1)))**2*tau_sn(nion+1+3*(i-1))/m_g(i)

    tau_sn(nion+2+3*(i-1))=tau_sn(nion+1+3*(i-1))          ! g-
    omega(nion+2+3*(i-1)) = q(nion+2+3*(i-1))*B/(m_g(i)*c_l)
    sigma(nion+2+3*(i-1)) = x(nion+2+3*(i-1))*nH*(q(nion+2+3*(i-1)))**2*tau_sn(nion+2+3*(i-1))/m_g(i)
end do


! Conductivities
sigP=0.d0
sigO=0.d0
sigH=0.d0
do  i=1,nvar
    sigP=sigP+sigma(i)
    sigO=sigO+sigma(i)/(1.d0+(omega(i)*tau_sn(i))**2)
    sigH=sigH-sigma(i)*omega(i)*tau_sn(i)/(1.d0+(omega(i)*tau_sn(i))**2)
end do

end subroutine resist
