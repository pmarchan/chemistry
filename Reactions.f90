subroutine reactions
use variables
implicit none

!!!! boucles
integer   :: i,j,k
double precision :: logt

logt = dlog10(T)

!!!! Charged species
! 1 : electrons
! 2 : M+
! 3 : m+
! 4 : H3+
! 5 : H+
! 6 : C+
! 7 : He+
! 8 : K+
! 9 : g+
! 10: g-
! 11: g0
! etc.
!!!! neutrals
! Nvar+1 : H2
! Nvar+2 : He
! Nvar+3 : M
! Nvar+4 : O
! Nvar+5 : 02
! Nvar+6 : C
! Nvar+7 : K

! Reactions rates of reactions from  Umebayashi & Nakano (1990)
! Reactions between species
Ba = (1.08d-11 * T**0.517 + 4d-10*T**(0.00669d0))*dexp(-227d0/T)     ! H + O
Bb = 1.29d-9                                                         ! H + O2
Bc = 1.1d-9                                                          ! H + M
Bd = 3.2d-14*exp(-35d0/T)                                            ! He + H2
Be = 1.4d-9 * (T/3d2)**(-0.5d0)                                      ! He + CO
Bf = 1.1d-9                                                          ! He + O2
Bg = 1.36d-9*(T/300.)**(-0.14)                                       ! H3 + CO 
Bh = 7.98d-10*(T/300.)**(-0.16)*dexp(-1.41d0/T)                      ! H3 + O
Bi = 9.3d-10                                                         ! H3 + O2
Bj = 1.0d-9                                                          ! H3 + M
Bk = 4.0d-16 *(T/3d2)**(-0.2d0)                                      ! C + H2
Bl = 3.42d-10                                                        ! C + O2
Bm = 4.54d-10                                                        ! C + O2 bis
Bn = 1.1d-9                                                          ! C + M
Bo = 2.9d-9                                                          ! M + m

! Electron recombinations
Bp = 1.269d-13*(315614d0/T)**1.502d0 * (1d0+(604625d0/T)**0.470d0)**(-1.923d0)    ! H
Bq = 1d-11 * T**0.5d0 *(12.72d0-1.615d0*logt - 0.3162d0*logt**2d0 + 0.0493d0*logt**3d0)    ! He
Br = 4.36d-8*(T/300.)**(-0.52)   ! H3
Bs = 2.34d-8*(T/300.)**(-0.52)   ! H3 bis
Bt = 9.62d-8*(T/3d2)**(-1.37d0) * dexp(-115786.2d0/T)
if(T <= 21140d0) Bt = 1.23d-17 * (T/3d2)**2.49d0 * dexp(21845.6d0/T)
if(T <= 7950d0) Bt = 4.67d-12*(T/3d2)**(-5d-1)   ! C
Bu = 2.75d-7*(300.d0/T)**0.64    ! m
Bv = 2.78d-12*(300.d0/T)**0.68   ! M

! Ionisation
G1 = 0.98d0   ! H2
G2 = 0.02d0   ! H2 bis
G3 = 0.53d0   ! He
G4 = 4.1d-15*xH2*nH/xi*dsqrt(T/1000d0)*dexp(-5.04d4/T)   ! K
G5 = 2d-10*xH2*nH/xi*dsqrt(T)*dexp(-15.8d4/T)
G6 = 1.4d-15*nH/xi*dsqrt(T)*dexp(-6.0d4/T)   ! Na


!!!! alp : creating i with j
alp(:,:) = 0.d0
alp(1,Nvar+1) = G1+G2
alp(1,Nvar+2) = G3
alp(4,Nvar+1) = G1
alp(5,Nvar+1) = G2
alp(7,Nvar+2) = G3
alp(8,Nvar+7) = G4
alp(1,Nvar+7) = G4
alp(5,Nvar+1) = alp(5,Nvar+1) + G5         ! H+
alp(1,Nvar+1) = alp(1,Nvar+1) + G5         ! H+
alp(9,Nvar+8) = G6
alp(1,Nvar+8) = G6


!!!! gam : destruction of i with reaction i + j -> ...
gam(:,:) = 0.d0
gam(5,1)  = Ba
gam(5,Nvar+5) = Bb
gam(5,Nvar+3) = Bc
gam(5,Nvar+7) = Bc
gam(5,Nvar+8) = Bc
gam(7,Nvar+1)  = Bd
gam(7,Nvar+6) = Be
gam(7,Nvar+5) = Bf
gam(4,Nvar+6) = Bg
gam(4,Nvar+4) = Bh
gam(4,Nvar+5) = Bi
gam(4,Nvar+3) = Bj
gam(4,Nvar+7) = Bj
gam(4,Nvar+8) = Bj
gam(6,Nvar+1)  = Bk
gam(6,Nvar+5) = Bl+Bm
gam(6,Nvar+3) = Bn 
gam(3,Nvar+3) = Bo
gam(6,Nvar+7) = Bn 
gam(3,Nvar+7) = Bo
gam(6,Nvar+8) = Bn 
gam(3,Nvar+8) = Bo
gam(5,1)  = Bp
gam(7,1)  = Bq
gam(4,1)  = Br+Bs
gam(6,1)  = Bt
gam(3,1)  = Bu
gam(2,1)  = Bv
gam(8,1)  = Bv
gam(9,1)  = Bv

!!!! bet : creation of i with j + k
bet(:,:,:) = 0.d0
bet(2,5,Nvar+3) = Bc 
bet(2,4,Nvar+3) = Bj
bet(2,6,Nvar+3) = Bn
bet(2,3,Nvar+3) = Bo
bet(8,5,Nvar+7) = Bc 
bet(8,4,Nvar+7) = Bj
bet(8,6,Nvar+7) = Bn
bet(8,3,Nvar+7) = Bo
bet(9,5,Nvar+7) = Bc 
bet(9,4,Nvar+7) = Bj
bet(9,6,Nvar+7) = Bn
bet(9,3,Nvar+7) = Bo
bet(3,5,Nvar+4) = Ba
bet(3,5,Nvar+5) = Bb
bet(3,7,Nvar+5) = Bf
bet(3,4,Nvar+6) = Bg
bet(3,4,Nvar+4) = Bh
bet(3,4,Nvar+5) = Bi
bet(3,6,Nvar+1) = Bk
bet(3,6,Nvar+5) = Bl+bm
bet(5,7,Nvar+1) = Bd
bet(6,7,Nvar+6) = Be


!!!! For the grains
! Charges
q(:)=1.d0*e    ! cations
q(1)=-1.d0*e   ! electron
do  i=nion+1,Nvar
    if (mod(i-nion-1,3)==0) q(i)=1.d0*e   ! g+
    if (mod(i-nion-1,3)==1) q(i)=-1.d0*e  ! g-
    if (mod(i-nion-1,3)==2) q(i)=0.d0     ! g0
end do

! Masses
m(:)=0.d0
m(1) = me           ! e-
m(2) = 23.5d0*mp    ! metal ions
m(3) = 29.d0*mp     ! molecular ions
m(4) = 3*mp         ! H3+
m(5) = mp           ! H+
m(6) = 12.d0*mp     ! C+
m(7) = 4.d0*mp      ! He+
m(8) = 39.098d0*mp ! K+
m(9) = 22.99d0*mp ! K+
do  i=1,alpha       ! grains
    m_g(i)=4.d0/3.d0*pi*r_g(i)**3*rho_s
    m(nion+1+3*(i-1))=m_g(i)
    m(nion+2+3*(i-1))=m_g(i)
    m(nion+3+3*(i-1))=m_g(i)
end do



! Reactions rates for the grains (Umebayashi & Nakano 1990)
do  j=1,alpha
    do  i=1,nion 

        if  (q(i) < 0.d0) then  ! Reactions g+ or g0 + negatives species (electrons)
           bet(nion+2+3*(j-1),nion+3+3*(j-1),i)=pi*r_g(j)**2*sqrt(8.d0*kb*T/(pi*m(i)))&
                                                &*(1.d0+(pi*e**2/(2.d0*r_g(j)*kb*T))**0.5)*P_e     ! with neutral grains to form g-
           gam(nion+1+3*(j-1),i)=pi*r_g(j)**2*sqrt(8.d0*kb*T/(pi*m(i)))*(1.d0+(e**2/(r_g(j)*kb*T)))&
                        &*(1.d0+(2.d0/(2.d0+(r_g(j)*kb*T/e**2))**0.5))*P_e   ! Reaction e- + g+ -> g0
           gam(nion+3+3*(j-1),i)=bet(nion+2+3*(j-1),nion+3+3*(j-1),i)   ! Reaction e- + g0 -> g-


        else if  (q(i) > 0.d0) then   ! Reactions g- ou g0 + positive species
           bet(nion+1+3*(j-1),nion+3+3*(j-1),i)=pi*r_g(j)**2*sqrt(8.d0*kb*T/(pi*m(i)))&
                                                &*(1.d0+(pi*e**2/(2.d0*r_g(j)*kb*T))**0.5)*P_i     ! With neutral grains to form g+
           gam(nion+2+3*(j-1),i)=pi*r_g(j)**2*sqrt(8.d0*kb*T/(pi*m(i)))*(1.d0+(e**2/(r_g(j)*kb*T)))&
                        &*(1.d0+(2.d0/(2.d0+(r_g(j)*kb*T/e**2))**0.5))*P_i   ! Reaction i+ + g- -> g0
           gam(nion+3+3*(j-1),i)=bet(nion+1+3*(j-1),nion+3+3*(j-1),i)   ! Reaction i+ + g0 -> g+


        end if
    end do


    ! Thermionic emission from grains
    ! g0 -> g+ + e-
    alp(1,nion+3+3*(j-1)) = 0.5 * 4d0*pi*r_g(j)**2d0 *&
                 & 4d0*pi*me*(kb*T)**2d0/(hp**3d0)*exp(-(5d0 + (-0d0)*e*e/r_g(j)/(1.6d-19*1d7)) * 1.6d-19*1d7 / (kb*T)) /xi
    alp(nion+1+3*(j-1),nion+3+3*(j-1)) = alp(1,nion+3+3*(j-1))
    ! g- -> g0 + e-
    alp(1,nion+2+3*(j-1)) = 0.5 * 4d0*pi*r_g(j)**2d0 *&
                 & 4d0*pi*me*(kb*T)**2d0/(hp**3d0)*exp(-(5d0 + (-1d0)*e*e/r_g(j)/(1.6d-19*1d7)) * 1.6d-19*1d7 / (kb*T)) /xi
 !   print*,alp(1,nion+2+3*(j-1)), e*e/(r_g(j))*1d-7/(1.6d-19)
    !alp(nion+3+3*(j-1),nion+2+3*(j-1)) = alp(1,nion+2+3*(j-1))

end do

! Reactions g+ + g- -> 2g0
do  i=1,alpha
    do  j=1,alpha
        gam(nion+1+3*(i-1),nion+2+3*(j-1))=pi*(r_g(i)+r_g(j))**2*sqrt(8.d0*kb*T/(pi*(m_g(i)*m_g(j)/(m_g(i)+m_g(j)))))*&
          &(1.d0+(e**2/((r_g(i)+r_g(j))*kb*T)))*(1.d0*(2.d0/(2.d0+((r_g(i)+r_g(j))*kb*T/e**2)))**0.5)

    end do
end do

! Reaction of charge transfert
do  i=1,alpha
    do  j=1,alpha
        gam(nion+1+3*(i-1),nion+3+3*(j-1))=pi*(r_g(i)+r_g(j))**2*sqrt(8.d0*kb*T/(pi*(m_g(i)*m_g(j)/(m_g(i)+m_g(j)))))*&
          &(1.d0+(pi*e**2/(2.d0*(r_g(i)+r_g(j))*kb*T))**0.5)*(0.5)  ! g+ + g0 -> same
        gam(nion+2+3*(i-1),nion+3+3*(j-1))=pi*(r_g(i)+r_g(j))**2*sqrt(8.d0*kb*T/(pi*(m_g(i)*m_g(j)/(m_g(i)+m_g(j)))))*&
          &(1.d0+(pi*e**2/(2.d0*(r_g(i)+r_g(j))*kb*T))**0.5)*(0.5)  ! g- + g0 -> same
        bet(nion+2+3*(j-1),nion+2+3*(i-1),nion+3+3*(j-1)) = gam(nion+2+3*(i-1),nion+3+3*(j-1))
        bet(nion+1+3*(j-1),nion+1+3*(i-1),nion+3+3*(j-1)) = gam(nion+1+3*(i-1),nion+3+3*(j-1))

    end do
end do



! Symetrisation of gam and bet
do  i=1,Ntot
    do  j=1,Ntot
        if  (gam(i,j) .ne. 0.d0) gam(j,i) = gam(i,j)
        do  k=1,Ntot
            if  (bet(i,j,k) .ne. 0.d0) bet(i,k,j) = bet(i,j,k)
        end do
    end do
end do

! Permutation matrix
perm(:,:) = 0.0d0
Id(:,:) = 0.0d0
do  i=1,Nvar
    Id(i,i)=1.d0
    do  j=1,Nvar
        if (abs(i-j).eq.0) perm(i,j)=1.d0   
    end do
end do


end subroutine reactions
