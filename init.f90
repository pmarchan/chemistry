subroutine init()
! Initialisation of abundances
use variables
implicit none


fO2 = 0.7d0 ! O fraction in O_2 form

xH2 = 0.5d0
xHe = 8.5d-2
xC  = 4.2d-4*delta1
xO  = 6.9d-4*delta1*(1 - fO2)
xO2 = 0.5d0*6.9d-4*delta1*fO2
xM  = 8.4d-5*delta2
xK  = 2.2d-10 ! from Umebayashi & Nakano 2009

xNa = xK*10**1.15
xMg = 4.0d-5
xAl = 3.2d-6
xCa = 2.3d-6
xFe = 3.4d-5
xNi = 1.8d-6
xSi = 3.8d-5

xg = 1.d0/(4.d0/3.d0*pi*(rg)**3*3) * mp*(xNa*22.99d0+xMg*24.3d0+xAl*26.982d0+xCa*40.078d0 &
   & +xFe*55.845d0+xNi*58.693d0+xSi*28.086d0)
xg=8.9d-13

if (.not. restart) then
  x(2) = 1.d-6     ! M+
  x(3) = 3.d-9     ! m+
  x(4) = 2.d-8     ! H3+
  x(5) = 2.d-8     ! H+
  x(6) = 2.d-8     ! C+
  x(7) = 3.d-9     ! He+
  x(8) = x(2)*xK/xM  ! K+, same ionisation as M
  x(9)= x(8)*10**1.15d0  ! Na+, see Lequeux 1975
end if

x(1)=sum(x(2:nion)) ! Neutrality

x(Nvar+1) = xH2
x(Nvar+2) = xHe
x(Nvar+3)= xM
x(Nvar+4)= xO
x(Nvar+5)= xO2
x(Nvar+6)= xC
x(Nvar+7)= xK-x(8)
x(Nvar+8)=xNa - x(9)

x(Nvar+7)=max(1d-30,xK-x(8))
x(Nvar+8) = max(1d-30,xNA - x(9))
x(Nvar+3) = max(1d-30,xM - x(2))
x(Nvar+1) = 0.5d0*max(1d-30,1d0 - x(5))


call grains()

end subroutine init
