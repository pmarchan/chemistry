subroutine eos(nhy,T2)
implicit none

double precision :: scale_d,n_star,n_star2,n_star3,g_star,g_star2,g_star3
double precision, intent(in)  :: nhy
double precision, intent(out) :: T2
integer :: i

     !open (156, file = 'eos.txt',ACCESS='APPEND')
     scale_d = 3.87e-24
     n_star=3.84e-13
     n_star2=3.84e-8
     n_star3=3.84e-3
     g_star=0.4
     g_star2=-0.3
     g_star3=0.56667
      
     
     
     !!!!!!!  EOS  !!!!!!!!!!!
     ! nH in particles per cc
     T2 = 10. * sqrt( 1. + (nhy*scale_d/n_star)**(2.*g_star) ) &
                    * ( 1. + (nhy*scale_d/n_star2))**(g_star2) &
                    * ( 1. + (nhy*scale_d/n_star3))**(g_star3)
     !write(156,*) nhy*scale_d,nhy,T2

     !close(156)
end subroutine eos


