double precision function calc_mag(rho)
  use variables
  IMPLICIT NONE
  integer :: sel,i
  double precision, intent(in) :: rho
  double precision :: atemp, btemp


  if(mag==0) then
    calc_mag = 1.43d-7*sqrt(rho)

  else if(mag==1) then
    if (rho < 1d6) then
      calc_mag = 1d-3*(rho/1d6)**0.5
    else
      calc_mag = 1d-3*(rho/1d6)**0.25
    end if


  else
    sel=4
    do i=1,4
      if(rho < rhom(6-i))  sel=5-i
    end do
    atemp=(log10(bmagm(sel+1))-log10(bmagm(sel)))/(log10(rhom(sel+1))-log10(rhom(sel)))
    btemp=log10(bmagm(sel))-atemp*log10(rhom(sel))
    calc_mag=10**(atemp*log10(rho)+btemp)

  end if

end function
