DOUBLE PRECISION function qte_grains(T)

! Compute the grains abundances at a given temperature
! Following Lenzuni,Gail,Henning 1995
! Carbon (C) -> 88.3%, from 750K to 1100K
! Silicates (MgFeSi04) -> 11.2%, from 1200K to 1300K
! Aluminium (Al2O3)-> 0.5%, from 1600K to 1700K
! Assumed to be linear

IMPLICIT NONE
DOUBLE PRECISION   :: T
DOUBLE PRECISION   :: T1C,T2C,T1Si,T2Si,T1Al,T2Al ! Evaporation temperature
DOUBLE PRECISION   :: frac_C,frac_Si,frac_Al      ! Fractions

T1C = 750.
T2C = 1100.
T1Si = 1200.
T2Si = 1300.
T1Al = 1600.
T2Al = 1700.

frac_C = 0.883
frac_Si = 0.112
frac_Al = 0.005


if(T < T1C) then
  qte_grains=1.
else
  if (T < T2C) then
    qte_grains=1.-(T-T1C)*frac_C/(T2C-T1C)
  else
    if(T < T1Si) then
      qte_grains = 1.-frac_C
    else
      if(T < T2Si) then
        qte_grains=1.-frac_C-(T-T1Si)*frac_Si/(T2Si-T1Si)
      else
        if(T < T1Al) then
          qte_grains=1.-frac_C-frac_Si
        else
          if(T < T2Al) then
            qte_grains=max(1.-frac_C-frac_Si-(T-T1Al)*frac_Al/(T2Al-T1Al),1d-20) 
          else
            qte_grains=1d-20
          end if
        end if
      end if
    end if
  end if
end if




end function qte_grains
