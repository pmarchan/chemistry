program abondances
use variables
implicit none

double precision :: tolx,tolf,errx,errf     ! Tolerance for Newton-Raphson
double precision :: ti,dt,dt0,dtemp         ! Time and timestep
double precision :: T0                      ! Temperature
double precision :: xg0_tot,xgp_tot,xgm_tot ! Grains total abundance
double precision :: ionis                   ! Ionisation
double precision :: nt,nd,mult              ! step in density or temperature (log)
double precision  :: qte_grains,qte_grains_prev  ! Grain evaporation
integer  :: i,j,k,l,ttt,j2,kk,lstart,Tstart

! resistivites
double precision  :: sigP,sigO,sigH      ! Conductivities parallel, orthogonal and hall
double precision  :: etaO,etaAD,etaH     ! Resistivities Ohm, ambipolar and Hall
double precision  :: B,calc_mag          ! Magnetic field
character(80)     :: infile

double precision :: dum

double precision :: prec=1d-5            ! Stopping criteria for Newton-Raphson : dt/(total time) < prec
double precision :: prec_jco=1d-3        ! Maximum allowed variation in one time step
integer :: ntrial = 1000000              ! Number of iterations for Newton-Raphson

call getarg(1,infile)

!Read parameters file
call read_params(infile)


print*, '******************************************************'
print*, '             Chemistry code starting                  '
print*
print*, 'Number of bins =',alpha
print*, 'EOS =',act_eos
print*
print*
print*
print*

! Initialisation 

nion = 9
Nvar = nion + 3*alpha
Ntot = Nvar + nion-1


call alloc()


qte_grains_prev=1.0d0

if(act_eos) Ttot=1
if(act_eos) ltot=762
Tstart = 1
T=Tmin


if(detail) open(unit=1,file='res.dat')

if(.NOT. restart) then
  open(unit=2,file='resnh.dat')
  write(2,*)ltot, Ttot, nvar
  write(2,*)'nH,x(1:nvar),xgp_tot,xgm_tot,xg0_tot,ti,T'
  write(2,*)
  close(2)
  open(unit=3,file='res_sig.dat')
  write(3,*)
  close(3)
  lstart=1

else

  open(2,file='resnh.dat')
  read(2,*) ltot, Ttot, nvar
  read(2,*)
  read(2,*)
  do i=1,Ttot
    lstart = 1
    do j=1, ltot
      read(2,*,end=101) nH,x(1:nion),dum,dum,dum,x(nion+1:nvar),dum,T
      lstart=lstart+1
    end do
    Tstart=Tstart+1
    read(2,*,end=101)
  end do

101  qte_grains_prev = qte_grains(T)
  if(act_eos) then
    if (dlog10(nH) > 18) then
      nd=0.05
    else if(dlog10(nH) >15) then
      nd=0.005
    else
      nd=0.3
    end if
  else
    nd=(dlog10(rho_max)-dlog10(rho_min))/(dble(ltot))
  end if
  rho_min=nH*10**(nd)

end if


! 4 times the precision in temperature for 750 K<T<1700 K
if(act_eos .eqv. .FALSE.) then
  nt=dlog10(7.5d2/dble(Tmin))+4d0*dlog10(1.7d3/7.5d2)+dlog10(dble(Tmax)/1.7d3)
  nt=nt/dble(Ttot)
end if






! Loop over temperature (Tot=1 with EOS)
do ttt=Tstart,Ttot

!  Step in temperature, better precision at grain evaporation
   if (ttt>1) then
     if(act_eos .eqv. .FALSE.)  then
       if(active_vap) then
         if(T >= 750d0 .and. T < 1700d0) then
           mult=4d0
         else
           mult=1d0
         end if
         T=T*10**(nt/mult)
       else
          T=T*10**((dlog10(Tmax)-dlog10(Tmin))/dble(Ttot))
       end if
     end if
   end if


    nH = rho_min


    call init()      
    if (act_eos) call eos(nH,T)
    call reactions      ! Compute reactions rates
    call fonctions()    ! Compute jacobian 

    do  l=lstart,ltot    ! Loop over density
      
        call equil() ! ionisation equilibrium
        if(act_eos) call eos(nH,T)
 
        B=calc_mag(nH) ! Magnetic field

        open(unit=2,file='resnh.dat',ACCESS='APPEND')
        ti = 0.d0

        if(detail) then
          write(1,*)"t,x(:)"
          write(1,*)ti,x(1:nvar)
        end if


        do  i=1,ntrial   ! Runs until the convergence criteria is met, or i==ntrial
 


          ! Evolution functions
          call reactions      
          call Fonctions()

          dtemp=abs(x(1)/F(1))
          do kk=2,Nvar
            if (x(kk) > thresh_qte) then
              dtemp=min(abs(x(kk)/F(kk)),dtemp)
            end if
          end do
          dt = dtemp

          do  j=1,100                     ! Computation of the time step dt fitting an abundance variation lower than prec_jco
            dt = dt/2.d0
            call evol(dt)                 ! Computation of new abundances
            jco(1)=abs(deltx(1)/x(1))
            do kk=1,nvar
              if(x(kk)>thresh_qte) jco(1)=max(abs(deltx(kk)/x(kk)),jco(1))
            end do
            if(jco(1) < prec_jco) then       ! If the variation is small enough for each abundance, the corresponding time step is chosen
              exit
            end if
            if(j==100) then
              print*, 'Problem in dt'
              stop
            end if
          end do


          ! Grain evaporation
          if (active_vap) then
            do j=1,alpha
              do j2=nion+1,nion+3
                x(j2+(j-1)*3) = x(j2+(j-1)*3)*qte_grains(T)/qte_grains_prev
              end do
            end do
          end if
          qte_grains_prev=qte_grains(T)


          ! Ionisation equilibrium
          ionis = 0d0
          do j=1,nvar
            ionis = ionis + deltx(j)*q(j)/e
          end do
          ! Updating abundances
          do  j=2,Nvar
            if(x(j)> thresh_qte) x(j)=x(j)+deltx(j)
          end do
          x(1)=sum(x(2:nvar)*q(2:nvar)/e)

          ! Precision
          errf=0.d0
          errx=0.d0
          do  j=1,nvar
            errf=errf+abs(F(j))/nvar
            errx=errx+abs(deltx(j)/x(j))/nvar
          end do
          if (minval(x(:))<=0.d0) then
            print*, 'improve precision !', minval(x),minloc(x)
            stop
          end if
          if  (dt/ti < prec) then
            print*, "maxerror : ", maxval(abs(F(1:Nvar)*dt/x(1:Nvar))), 'dt : ',dt, 'err :', errx,errf
            if(detail) write(1,*)ti,x(1:nvar),nH,dt
            write(*,*)'nH : ', nh,', T ', T, 'Equilibrium OK', i
            exit
          end if


          ti=ti+dt
          if  (mod(i,100)==0) then
            if(detail) write(1,*)ti,x(1:nvar),nH,dt
          end if
          if  (mod(i,10000)==0) then
            write(*,*)'nH : ', nh,', T = ', T, 'Advancing : ',(ttt-1)*ltot + l,'/',ltot*Ttot,', iteration = ', i
          end if
          if  (mod(i,ntrial)==0) then
            print*, ''
            print*, "maxerror : ", maxval(abs(F(1:Nvar)*dt/x(1:Nvar))), 'dt : ',dt, 'errs :', errx,errf
          end if

          if (i == ntrial) then
            open(unit=5,file='errlog.dat',ACCESS='APPEND')
            write(5,*)"!!!!!!!!! WARNING, MAX NTRIAL REACHED !!!!!!!!", nH, T
            close(5)
            print*, 'Max iterations reached'
          end if

          x(Nvar+7)=max(1d-30,xK-x(8))
          x(Nvar+8) = max(1d-30,xNA - x(9))
          x(Nvar+3) = max(1d-30,xM - x(2))
          x(Nvar+1) = 0.5d0*max(1d-30,1d0 - x(5))

        end do
        
        print*, ''
        print*, (ttt-1)*ltot + l,'nH=',nH,'T =',T
        print*, ''
        
        if (detail) then
          write(1,*)
          write(1,*)
        end if


        ! Write total abundances
        xg0_tot = 0.d0
        xgp_tot = 0.d0
        xgm_tot = 0.d0
        do  i=1,alpha
            xg0_tot = xg0_tot+x(nion+3+(i-1)*3)
            xgp_tot = xgp_tot+x(nion+1+(i-1)*3)
            xgm_tot = xgm_tot+x(nion+2+(i-1)*3)
        end do
        write(2,*) nH,x(1:nion),xgp_tot,xgm_tot,xg0_tot,x(nion+1:nvar),ti,T
        close(2)

        ! Compute resistivities
        call resist(sigP,sigO,sigH,B)
        sigP=(sigP)
        sigO=(sigO)
        sigH=(sigH)
        etaO=1.d0/(sigP)
        etaAD=(sigO/(sigO**2+sigH**2)-1.d0/sigP)
        etaH=(sigH/(sigH**2+sigO**2))
        open(unit=3,file='res_sig.dat',ACCESS='APPEND')
        write(3,*)nH,B,sigP,sigO,sigH,etaAD,etaO,etaH,T
        close(3)


        ! Step in density
        if(act_eos) then
          if (dlog10(nh) > 18) then
            nd=0.05
            prec = 1d-7
          else if(dlog10(nh) >15) then
            nd=0.005
          else
            nd=0.3
          end if
        else
          nd=(dlog10(rho_max)-dlog10(rho_min))/(dble(ltot))
        end if
        nh=nh*10**(nd)


    end do

    open(unit=2,file='resnh.dat',ACCESS='APPEND')
    write(2,*)
    close(2)

    open(unit=3,file='res_sig.dat',ACCESS='APPEND')
    write(3,*)
    close(3)

    lstart = 1

end do

close(1)

end program abondances
