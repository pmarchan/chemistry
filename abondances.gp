set term post enh color 17
set output "plot_abundances.ps"
plot_1 = 'resnh.dat'
set format "10^{%L}"
set logscale
set xrange [300:1.e30]
set yrange [1.e-24:1.e-0]
set key at 1.e30,1.e-7
set xtics font "Times-Roman,22"
set ytics font "Times-Roman,22"

set object 1 rectangle from 6.e10,1.e-26 to 1.e17,1.e-0 behind
set object 1 rect fc rgb "#D3D3D3"

set object 2 rectangle from 3.e20,1.e-26 to 1.e30,1.e-0 behind
set object 2 rect fc rgb "#D3D3D3"

set label 'First collapse' at 1.e5,1.e-1
set label 'Isothermal' at 1.e5,1.e-2 
set label 'First core' at 1.e13,1.e-1 
set label 'Adiabatic' at 1.e13,1.e-2 
set label 'Second' at 2.e17,1.e-1 
set label 'collapse' at 2.e17,1.e-2 
set label 'Second core' at 1.e23,1.e-1
set label 'Adiabatic' at 1.e23,1.e-2 



set xlabel "n_H (cm^{-3})" font "Times-Roman,22"
set ylabel "Fractionnal abondances" font "Times-Roman,22"


plot plot_1 u 1:2 w l lt 1 lw 4 tit 'e^-',\
     plot_1 u 1:3 w l lt 3 lc 3 lw 3 tit 'M^+',\
     plot_1 u 1:4 w l lt 4 lc 5 lw 3 tit 'm^+',\
     plot_1 u 1:5 w l lt 5 lc 9 lw 3 tit 'H_3^+',\
     plot_1 u 1:6 w l lt 6 lc 3 lw 3 tit 'H^+',\
     plot_1 u 1:7 w l lt 7 lc 5 lw 3 tit 'C^+',\
     plot_1 u 1:8 w l lt 8 lc 9 lw 3 tit 'He^+',\
     plot_1 u 1:9 w l lt 9 lc 4 lw 3 tit 'K^+',\
     plot_1 u 1:10 w l lt 9 lc 6 lw 3 tit 'Na^+',\
     plot_1 u 1:11 w l lt 2 lc 2 lw 4 tit 'g^+',\
     plot_1 u 1:12 w l lt 2 lc 1 lw 4 tit 'g^-',\
     plot_1 u 1:13 w l lt 2 lc 0 lw 4 tit 'g^0'
     

