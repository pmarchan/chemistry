subroutine grains()
!This subroutine compute the grains abundances,
!and their size distribution
use variables
implicit none
integer   :: i
double precision :: lambda
double precision :: lp1,lp3,lp4

lambda=grains_pow_law
Lp1=dble(lambda+1)
Lp3=dble(lambda+3)
Lp4=dble(lambda+4)

rho_gtot=0.01d0*(a_min/a_0)*zeta**(-0.5)*rho_n_tot



if  (alpha==1) then
    rho_gtot=0.01d0*rho_n_tot
    xg=1.d0/nH*(rho_gtot/(4.d0/3.d0*pi*rho_s*a_0**3))
    if(.not. restart) then
      x(nion+1)=xg/3.d0
      x(nion+2)=xg/3.d0
      x(nion+3)=xg/3.d0
    end if
    r_g(1)=a_0
else

rho_gtot=1d-2*rho_n_tot*(a_min/a_0)*Lp3/Lp4*(1-zeta**(-Lp4))/(1-zeta**(-Lp3))
xg=1.d0/nH*(rho_gtot/(4.d0/3.d0*rho_s*pi*a_min**3))*&
 & (Lp4/Lp1)*(1.d0-zeta**(-Lp1))/(1.d0-zeta**(-Lp4))
do  i=1,alpha    ! cf Kunz & Mouschovias 2009
    x_g(i)=xg*zeta**(-dble(i-1)*Lp1/dble(alpha)) * &
       &  (1d0 - zeta**(-Lp1/dble(alpha)))/(1d0 - zeta**(-Lp1))
    r_g(i)=a_min*zeta**(-dble(i)/dble(alpha)) * &
         & dsqrt( Lp1/Lp3* (1d0-zeta**(Lp3/dble(alpha)))/(1d0-zeta**(Lp1/dble(alpha))))
    if(.not. restart) then
      x(nion+1+3*(i-1))=x_g(i)/3.d0
      x(nion+2+3*(i-1))=x_g(i)/3.d0
      x(nion+3+3*(i-1))=x_g(i)/3.d0
    end if
end do
end if

end subroutine grains
