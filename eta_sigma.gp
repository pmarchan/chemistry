reset
set term post enh color 22
set logscale
set xlabel 'n_{H} (cm^{-3})' font "Times-Roman,22"
set ylabel '{/Symbol h} (s)' font "Times-Roman,22"
set format "10^{%L}"
set xtics font "Times-Roman,20"
set ytics font "Times-Roman,20"
set xrange [1:1e27]
set yrange [1e-18:1e6]

set output 'plot_resistivities.ps'
set key top right


plot_1='res_sig.dat'


set object 1 rectangle from 6e10,1e-18 to 1.e17,1e6 behind
set object 1 rect fc rgb "#D3D3D3"

set object 2 rectangle from 3e20,1e-18 to 1.e27,1e6 behind
set object 2 rect fc rgb "#D3D3D3"

set label 'First collapse' at 1.e5,1.e5 font "Times-Roman,18"
set label 'Isothermal' at 1.e5,1.e4 font "Times-Roman,18"
set label 'First core' at 1.e13,1.e5  font "Times-Roman,18"
set label 'Adiabatic' at 1.e13,1.e4 font "Times-Roman,18" 
set label 'Second' at 2.e17,1.e5  font "Times-Roman,18"
set label 'collapse' at 2.e17,1.e4  font "Times-Roman,18"
set label 'Second core' at 1.e22,1.e5  font "Times-Roman,18"
set label 'Adiabatic' at 1.e22,1.e4  font "Times-Roman,18"

set label '{/Symbol h}_{AD}' at 5.e6,1.e-3 textcolor lt 1
set label '{/Symbol h}_{/Symbol W}' at 5.e6,1.e-9 textcolor lt 2
set label '-{/Symbol h}_{H}' at 3.e3,1.e-2 textcolor lt 5
set label '{/Symbol h}_{H}' at 2.e14,7.e-4 textcolor lt 3


plot plot_1 u 1:(($6)) w l lt 1 lw 2 lc 1 not,\
     plot_1 u 1:(($7)) w l lt 1 lw 2 lc 2 not,\
     plot_1 u 1:(($8)) w l lt 1 lw 2 lc 3 not,\
     plot_1 u 1:(-($8)) w l lt 1 lw 2 lc 5 not



reset


set term post enh color 22
set logscale
set xlabel 'n_{H} (cm^{-3})' font "Times-Roman,22"
set ylabel '{/Symbol s} (s^{-1})' font "Times-Roman,22"
set format "10^{%L}"
set xtics font "Times-Roman,20"
set ytics font "Times-Roman,20"
set xrange [1:1e27]
set yrange [1e-6:1e14]

set output 'plot_conductivities.ps'
set key bot right


plot_1='res_sig.dat'


set object 1 rectangle from 6e10,1e-18 to 1.e17,1e16 behind
set object 1 rect fc rgb "#D3D3D3"

set object 2 rectangle from 3e20,1e-18 to 1.e27,1e16 behind
set object 2 rect fc rgb "#D3D3D3"

set label 'First collapse' at 1.e5,1.e13 font "Times-Roman,18"
set label 'Isothermal' at 1.e5,1.e12 font "Times-Roman,18"
set label 'First core' at 1.e13,1.e13  font "Times-Roman,18"
set label 'Adiabatic' at 1.e13,1.e12 font "Times-Roman,18"
set label 'Second' at 3.e17,1.e13 font "Times-Roman,18"
set label 'collapse' at 3.e17,1.e12 font "Times-Roman,18"
set label 'Second core' at 1.e21,1.e13 font "Times-Roman,18"
set label 'Adiabatic' at 1.e21,1.e12 font "Times-Roman,18"


plot plot_1 u 1:(($3)) w l lt 1 lw 2 lc 1 tit '{/Symbol s}_{||}',  \
     plot_1 u 1:(($4)) w l lt 1 lw 2 lc 2 tit '{/Symbol s}_{/Symbol \136}', \
     plot_1 u 1:(($5)) w l lt 1 lw 2 lc 3 tit '{/Symbol s}_{H}',\
     plot_1 u 1:((-$5)) w l lt 1 lw 2 lc 5 tit '-{/Symbol s}_{H}'



